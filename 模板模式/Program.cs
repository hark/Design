﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 模板模式
{
    class Program
    {
        static void Main(string[] args)
        {
            CoffeeWithHook coffee = new CoffeeWithHook();
            Console.WriteLine("Making coffee...");
            coffee.prepareRecipe();
            Console.ReadLine();
        }
    }
}
