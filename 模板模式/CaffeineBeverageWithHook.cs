﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 模板模式
{
    public abstract class CaffeineBeverageWithHook
    {
        public void prepareRecipe()
        {
            boilWater();
            brew();
            pourInCup();
            if (customerWantsCondiments())
            {
                addCondiments();
            }
        }

        public abstract void brew();

        public abstract void addCondiments();

        void boilWater()
        {
            Console.WriteLine("Boiling water");
        }

        void pourInCup()
        {
            Console.WriteLine("Pouring into cup");
        }

        //钩子，子类可以覆盖这个方法
        public virtual bool customerWantsCondiments()
        {
            return true;
        }
    }
}
