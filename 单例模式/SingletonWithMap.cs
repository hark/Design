﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;


namespace 单例模式
{
    public class SingletonWithMap
    {
        private static Hashtable sinRegistry = new Hashtable();
        private static SingletonWithMap instance = new SingletonWithMap();

        protected SingletonWithMap()
        { 
        }

        public static SingletonWithMap getInstance(string name)
        {
            if (name == null)
            {
                name = "Singelton";
            }
            if (sinRegistry[name] == null)
            {
                try
                {
                    sinRegistry.Add(name,Activator.CreateInstance(typeof(SingletonWithMap)));
                }
                catch (Exception e)
                {
                    throw;
                }
            }
            return (SingletonWithMap)sinRegistry[name];
        }
    }
}
