﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 单例模式
{
    public class SingletonException:Exception
    {
        public SingletonException(string message)
            : base(message)
        { }
    }
}
