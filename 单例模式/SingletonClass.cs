﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 单例模式
{
    public class SingletonClass
    {
        public static SingletonClass Instance = null;

        private static readonly object locker = new object();

        private SingletonClass()
        {
        }

        public static SingletonClass GetInstance()
        {
            lock (locker)
            {
                if (Instance == null)
                {
                    System.Threading.Thread.Sleep(1000);
                    Instance = new SingletonClass();
                }
            }
            return Instance;
        }
    }
}
