﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 命令模式
{
    public class Light
    {
        public void On()
        {
            Console.WriteLine("The Light Is On");
        }

        public void Off()
        {
            Console.WriteLine("The Light Is Off");
        }
    }
}
