﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using MySql.Data.MySqlClient;


namespace 工厂模式
{
    public class MysqlFactory:IDBFactory
    {
        private string _strConnectionString = string.Empty;

        public MysqlFactory(string strConnectionString)
        {
            _strConnectionString = strConnectionString;
        }

        #region 根据连接字符串获得数据库连接对象
        public DbConnection GetConnection(string strConnectionString)
        {
            MySqlConnection _conn = new MySqlConnection(strConnectionString);
            if (_conn.State == ConnectionState.Closed)
            {
                _conn.Open();
            }
            return _conn;
        }
        #endregion

        #region 获得数据库连接对象
        public DbConnection GetConnection()
        {
            MySqlConnection _conn = new MySqlConnection(_strConnectionString);
            if (_conn.State == ConnectionState.Closed)
            {
                _conn.Open();
            }
            return _conn;
        }
        #endregion

        #region 根据名称和值获得数据库参数对象
        public DbParameter GetParameter(string name, object value)
        {
            MySqlParameter _parm = new MySqlParameter(name, value);
            return _parm;
        }
        #endregion

        #region 根据名称和类型获得数据库参数对象
        public DbParameter GetParameter(string name, DbType type)
        {
            MySqlParameter _parm = new MySqlParameter(name, type);
            return _parm;
        }
        #endregion

        #region 根据名称、类型和值获得数据库参数对象
        public DbParameter GetParameter(string name, DbType type, object value)
        {
            MySqlParameter _parm = new MySqlParameter(name, type);
            _parm.Value = value;
            return _parm;
        }
        #endregion

        public System.Data.Common.DbCommand GetCommand(System.Data.Common.DbConnection conn, string strSql, params System.Data.Common.DbParameter[] parms)
        {
            throw new NotImplementedException();
        }

        public System.Data.Common.DbCommand GetCommand(string strSql, params System.Data.Common.DbParameter[] parms)
        {
            throw new NotImplementedException();
        }

        public int ExecuteNonQuery(string strSql, params System.Data.Common.DbParameter[] parms)
        {
            throw new NotImplementedException();
        }

        public int ExecuteNonQuery(System.Data.Common.DbConnection conn, string strSql, params System.Data.Common.DbParameter[] parms)
        {
            throw new NotImplementedException();
        }

        public int ExecuteNonQuery(System.Data.Common.DbTransaction tran, string strSql, params System.Data.Common.DbParameter[] parms)
        {
            throw new NotImplementedException();
        }

        public System.Data.Common.DbDataReader ExecuteDataReader(string strSql, params System.Data.Common.DbParameter[] parms)
        {
            throw new NotImplementedException();
        }

        public System.Data.Common.DbDataReader ExecuteDataReader(System.Data.Common.DbConnection conn, string strSql, params System.Data.Common.DbParameter[] parms)
        {
            throw new NotImplementedException();
        }

        public System.Data.Common.DbDataReader ExecuteDataReader(System.Data.Common.DbTransaction tran, string strSql, params System.Data.Common.DbParameter[] parms)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetDataTable(string strSql, params System.Data.Common.DbParameter[] parms)
        {
            throw new NotImplementedException();
        }

        public DataTable GetDataTable(DbConnection conn, string strSql, params DbParameter[] parms)
        {
            try
            {
                DataTable dt = new DataTable();
                MySqlConnection _conn = conn as MySqlConnection;
                MySqlCommand _cmd = new MySqlCommand(strSql, _conn);
                MySqlParameter[] _parms = Array.ConvertAll(parms, parm => { return parm as MySqlParameter; });
                _cmd.Parameters.AddRange(_parms);
                MySqlDataAdapter _sda = new MySqlDataAdapter(_cmd);
                _sda.Fill(dt);
                return dt;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public System.Data.DataTable GetDataTable(System.Data.Common.DbTransaction tran, string strSql, params System.Data.Common.DbParameter[] parms)
        {
            throw new NotImplementedException();
        }

    }
}
