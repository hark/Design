﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;

namespace 工厂模式
{
    interface IDBFactory
    {
         DbConnection GetConnection(string strConnectionString);
         DbConnection GetConnection();
         DbParameter GetParameter(string name,object value);
         DbParameter GetParameter(string name, DbType type);
         DbParameter GetParameter(string name,DbType type,object value);
         DbCommand GetCommand(DbConnection conn, string strSql, params DbParameter[] parms);
         DbCommand GetCommand(string strSql, params DbParameter[] parms);
         int ExecuteNonQuery(string strSql,params DbParameter[] parms);
         int ExecuteNonQuery(DbConnection conn, string strSql, params DbParameter[] parms);
         int ExecuteNonQuery(DbTransaction tran, string strSql, params DbParameter[] parms);
         DbDataReader ExecuteDataReader(string strSql, params DbParameter[] parms);
         DbDataReader ExecuteDataReader(DbConnection conn,string strSql,params DbParameter[] parms);
         DbDataReader ExecuteDataReader(DbTransaction tran,string strSql,params DbParameter[] parms);
         DataTable GetDataTable(string strSql,params DbParameter[] parms);
         DataTable GetDataTable(DbConnection conn, string strSql, params DbParameter[] parms);
         DataTable GetDataTable(DbTransaction tran,string strSql,params DbParameter[] parms);
    }
}
