﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Reflection;

namespace 工厂模式
{
    class Program
    {
        public static readonly string strConnectionString =ConfigurationManager.AppSettings["ConnectionString"];
        public static readonly string strDbType = ConfigurationManager.AppSettings["DBType"];
        public static readonly string strDbFactory=ConfigurationManager.AppSettings["DBFactory"];
        static void Main(string[] args)
        {
            //IDBFactory factory = GetFactory();
            IDBFactory factory = GetFactory(strDbFactory);
            string strSql = "select * from sys_company";
            DbConnection conn = factory.GetConnection(strConnectionString);
            DataTable dt = factory.GetDataTable(conn, strSql);
            foreach (DataRow row in dt.Rows)
            {
                Console.WriteLine(row["companyName"]);
            }
            Console.Read();
        }

        static IDBFactory GetFactory()
        {
            if (strDbType == "mssql")
            {
                return new MssqlFactory(strConnectionString);
            }
            else
            {
                return null;
            }
        }

        static IDBFactory GetFactory(string name)
        {
            IDBFactory factory = Assembly.GetExecutingAssembly().CreateInstance(name,false, BindingFlags.Default,null,new object[]{strConnectionString},null,null) as IDBFactory;
            return factory;
        }
    }
}
