﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 生成器模式
{
    class Director
    {
        public void Construct(IBuilder builder)
        {
            builder.BuilderPartA();
            builder.BuilderPartB();
            builder.BuilderPartB();
        }
    }
}
