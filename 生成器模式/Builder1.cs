﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 生成器模式
{
    class Builder1:IBuilder
    {
        private Product product = new Product();
        public void BuilderPartA()
        {
            product.Add("PartA");
        }

        public void BuilderPartB()
        {
            product.Add("PartB");
        }

        public Product GetResult()
        {
            return product;
        }
    }
}
