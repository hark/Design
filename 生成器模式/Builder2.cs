﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 生成器模式
{
    class Builder2:IBuilder
    {
        private Product product = new Product();

        public void BuilderPartA()
        {
            product.Add("PartX");
        }

        public void BuilderPartB()
        {
            product.Add("PartY");
        }

        public Product GetResult()
        {
            return product;
        }
    }
}
