﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 策略模式
{
    public class Dock
    {
        protected Flyable flyable;

        public void setFlyable(Flyable flyable)
        {
            this.flyable = flyable;
        }

        public virtual void fly()
        {
            this.flyable.Flying();
        }
    }
}
