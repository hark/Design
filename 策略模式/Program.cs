﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 策略模式
{
    class Program
    {
        static void Main(string[] args)
        {
            Dock dock = new ModleDuck();
            dock.fly();
            dock.setFlyable(new FlyWithWings());
            dock.fly();
            Console.In.Read();
        }
    }
}
