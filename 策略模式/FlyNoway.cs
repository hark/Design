﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 策略模式
{
    public class FlyNoway:Flyable
    {
        public override void Flying()
        {
            Console.WriteLine("Can`t flying");
        }
    }
}
