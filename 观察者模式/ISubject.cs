﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 观察者模式
{
    public interface ISubject
    {
        void registerObserver(IObserver observer);
        void removeObserver(IObserver observer);
        void notifyObservers();
    }
}
