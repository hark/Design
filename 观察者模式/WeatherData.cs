﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace 观察者模式
{
    public class WeatherData:ISubject
    {
        private ArrayList observers;
        private float temperature;
        private float humidity;
        private float pressure;
        public WeatherData()
        {
            observers = new ArrayList();
        }

        #region ISubject 成员

        public void registerObserver(IObserver observer)
        {
            this.observers.Add(observer);
        }

        public void removeObserver(IObserver observer)
        {
            if (this.observers.Contains(observer))
            {
                this.observers.Remove(observer);
            }
        }

        public void notifyObservers()
        {
            foreach (IObserver observer in this.observers)
            {
                observer.update(temperature, humidity, pressure);
            }
        }

        #endregion

        public void measureMentsChange()
        {
            notifyObservers();
        }

        public void setMessureMents(float temperature, float humidity, float pressure)
        {
            this.temperature = temperature;
            this.humidity = humidity;
            this.pressure = pressure;
            measureMentsChange();
        }
    }
}
