﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 观察者模式
{
    public class CurrentConditionsDisplay:IObserver,IDisplayElement
    {
        private float temperature;
        private float humidity;
        private ISubject weatherData;

        public CurrentConditionsDisplay(ISubject weatherData)
        {
            this.weatherData = weatherData;
            weatherData.registerObserver(this);
        }

        #region IObserver 成员

        public void update(float temperature, float humidity, float pressure)
        {
            this.temperature = temperature;
            this.humidity = humidity;
            display();
        }

        #endregion

        #region IDisplayElement 成员

        public void display()
        {
            Console.Out.WriteLine("Current Conditions:" + temperature + " F degrees and " + humidity + "% humidity");
        }

        #endregion
    }
}
