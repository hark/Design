﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 观察者模式
{
    class Program
    {
        static void Main(string[] args)
        {
            WeatherData weatherData = new WeatherData();
            IObserver observer = new CurrentConditionsDisplay(weatherData);

            weatherData.setMessureMents(50, 30, 34.4F);

            Console.In.Read();
        }
    }
}
